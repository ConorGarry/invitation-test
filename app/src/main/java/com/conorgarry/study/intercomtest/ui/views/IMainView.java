package com.conorgarry.study.intercomtest.ui.views;

import com.conorgarry.study.intercomtest.models.Person;

import java.util.List;

/**
 * Created by conor on 13/11/2015.
 */
public interface IMainView {
    void showProgress();
    void hideProgress();
    void updateList(List<Person> personList);
    void showInviteDialog(String name, int distance);
}
