package com.conorgarry.study.intercomtest.ui.presenters;

import android.text.format.DateUtils;

import com.conorgarry.study.intercomtest.App;
import com.conorgarry.study.intercomtest.models.Person;
import com.conorgarry.study.intercomtest.ui.views.IMainView;
import com.conorgarry.study.intercomtest.utils.Cons;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okio.BufferedSource;
import okio.Okio;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by conor on 13/11/2015.
 */
public class MainPresenter implements IMainPresenter {

    private IMainView mMainView;
    private static Gson sGson = new GsonBuilder().create();
    private List<Person> mFilteredList = new ArrayList<>();

    private Subscription mSubscription;

    public MainPresenter(WeakReference<IMainView> mainView) {
        mMainView = mainView.get();
    }

    // Get json from assets
    // Parse list with Gson
    // Sort by ascending
    // Filter Persons less than 100km away
    // Update Ui with filtered list.
    @Override
    public void initGeoInvitation(String filePath) {
        mMainView.showProgress();

       mSubscription =  Observable.just(getJson(filePath))
                .subscribeOn(Schedulers.io())
                .flatMap(s -> Observable.just(parseJson(s)))
                .flatMap(persons -> Observable.just(sortByAsc(persons)))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(persons -> {
                    Observable.from(persons)
                            .subscribe(new Subscriber<Person>() {
                                @Override
                                public void onCompleted() {
                                    mMainView.updateList(mFilteredList);
                                    mMainView.hideProgress();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    mMainView.hideProgress();
                                }

                                @Override
                                public void onNext(Person person) {
                                    person.setDistance((int) distanceFromOffice(person));
                                    if (person.getDistance() < Cons.MAX_DISTANCE) {
                                        Timber.i(String.format("Adding: %s", person));
                                        mFilteredList.add(person);
                                    } else {
                                        Timber.w(String.format("Removing: %s", person));
                                    }
                                }
                            });
                });
    }

    @Override
    public String getJson(String filePath) {
        String json;
        try {
            InputStream is = App.get().getAssets().open(filePath);
            BufferedSource source = Okio.buffer(Okio.source(is));
            json = source.readUtf8();
        } catch (IOException e) {
            Timber.e(String.format("Error getting json file: %s", filePath));
            e.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public List<Person> parseJson(String s) {
        Type listType = new TypeToken<ArrayList<Person>>(){}.getType();
        try { // Sleep for half second to mimic network call time and show progress spinner.
            Thread.sleep(DateUtils.SECOND_IN_MILLIS / 2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return sGson.fromJson(s, listType);
    }

    @Override
    public List<Person> sortByAsc(List<Person> personList) {
        Collections.sort(personList, (lhs, rhs) ->
                ((Integer) lhs.getUserId()).compareTo(rhs.getUserId()));
        return personList;
    }

    @Override
    public double distanceFromOffice(Person person) {
        double lat1 = 0, lat2 = 0;
        double dLat = toRadians(Double.parseDouble(person.getLatitude()) - Cons.DUB_OFFICE_LAT);
        double dLon = toRadians(Double.parseDouble(person.getLongitude()) - Cons.DUB_OFFICE_LON);
        lat1 = toRadians(lat1);
        lat2 = toRadians(lat2);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return Cons.RADIUS_EARTH * c;
    }

    @Override
    public List<Person> getFilteredList() {
        return mFilteredList;
    }

    @Override
    public void unSubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            Timber.d("Unsubscribing.");
            mSubscription.unsubscribe();
        }
    }

    @Override
    public double toRadians(double deg) {
        return deg * (Math.PI / 180);
    }
}
