package com.conorgarry.study.intercomtest.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.view.View;

import com.conorgarry.study.intercomtest.App;
import com.conorgarry.study.intercomtest.adapters.InviteAdapter;
import com.conorgarry.study.intercomtest.utils.UiUtils;

/**
 * Created by conor on 14/11/2015.
 * ListView cell containing the User data.
 * Adapter: {@link InviteAdapter}
 */
public class InviteeCell extends View {

    private TextPaint mTpId = new TextPaint(Paint.ANTI_ALIAS_FLAG);
    private TextPaint mTpName = new TextPaint(Paint.ANTI_ALIAS_FLAG);
    private TextPaint mTpDistance = new TextPaint(Paint.ANTI_ALIAS_FLAG);
    private int[] mPosId = new int[2], mPosName = new int[2], mPosDist = new int[2];

    private int mId, mDistance;
    private String mName;
    private int mPadding = UiUtils.getFullScreenMarginByPercent(getContext());

    public InviteeCell(Context context) {
        super(context);
        initView();
        mTpDistance.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
//        mTpDistance.setTextSize((float) (mTpDistance.getTextSize()));
    }

    public void setUserData(int id, int distance, String name) {
        mId = id;
        mDistance = distance;
        mName = name;

        // Set distance colour based on value, extra green tint for closer proximity.
        mTpDistance.setColor(Color.argb(255, 160, 200 - distance, 24));
    }

    private void initView() {
        initTextPaint(mTpId, mTpName, mTpDistance);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), mPadding * 4);
        mPosId[0] = mPadding / 2;
        mPosId[1] = (int) ((getMeasuredHeight() / 2) - (mTpId.descent() + mTpId.ascent()) / 2);
        mPosName[0] = mPosId[0] + mPadding * 3;
        mPosDist[0] = getMeasuredWidth() - (mPadding * 4);
    }

    /** Init text size and colour of id and name **/
    private void initTextPaint(TextPaint... textPaints) {
        for (TextPaint tp : textPaints) {
            tp.setTextSize(UiUtils.spToPx(App.get(), (int) (mPadding * 0.7)));
            tp.setColor(Color.BLACK);
            tp.setTextAlign(Paint.Align.LEFT);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawText(String.valueOf(mId), mPosId[0], mPosId[1], mTpId);
        canvas.drawText(mName, mPosName[0], mPosId[1], mTpName);
        canvas.drawText(String.valueOf(mDistance), mPosDist[0], mPosId[1], mTpDistance);
    }
}
