package com.conorgarry.study.intercomtest.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.conorgarry.study.intercomtest.App;
import com.conorgarry.study.intercomtest.R;
import com.conorgarry.study.intercomtest.utils.UiUtils;

/**
 * Created by conor on 14/11/2015.
 */
public class DialogFactory {

    public static class DialogInvite extends DialogFragment {

        public static DialogInvite newInstance(String text, int distance) {
            DialogInvite d = new DialogInvite();
            Bundle b = new Bundle();
            b.putString("name", text);
            b.putInt("distance", distance);
            d.setArguments(b);
            return d;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final String name = getArguments().getString("name");
            final int distance = getArguments().getInt("distance");
            return new AlertDialog.Builder(getActivity())
                    .setIcon(null)
                    .setTitle(App.getString(App.get(), R.string.dialog_invite_title))
                    .setMessage(String.format(App.getString(App.get(), R.string.dialog_invite_body), name, distance))
                    .setPositiveButton("OK", (dialog, which) -> {
                        UiUtils.showToast(String.format("%s invited.", name));
                    }).setNegativeButton("Cancel", null)
                    .create();

        }
    }
}
