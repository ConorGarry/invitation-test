package com.conorgarry.study.intercomtest;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;

import timber.log.Timber;

/**
 * Created by conor on 13/11/2015.
 */
public class App extends Application {

    private static App sInstance;
    public static App get() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Static reference to class.
        sInstance = this;

        // init Timber
        Timber.plant(new Timber.DebugTree());
    }

    // Resource access
    public static Resources getResources(Context ctx) {
        return ctx.getResources();
    }

    public static float getDimension(Context ctx, int dimenId) {
        return getResources(ctx.getApplicationContext()).getDimension(dimenId);
    }

    public static String getString(Context ctx, int stringId) {
        return getResources(ctx.getApplicationContext()).getString(stringId);
    }

    public static String getString(Context ctx, int stringId, Object... stringArgs) {
        return getResources(ctx.getApplicationContext()).getString(stringId, stringArgs);
    }

    public static String[] getStringArray(Context ctx, int stringArrayId) {
        return getResources(ctx.getApplicationContext()).getStringArray(stringArrayId);
    }

    public static int[] getIntArray(Context ctx, int intArrayId) {
        return getResources(ctx.getApplicationContext()).getIntArray(intArrayId);
    }

    public static AssetManager getAssets(Context ctx) {
        return getResources(ctx.getApplicationContext()).getAssets();
    }

    public static Drawable getDrawable(Context ctx, int stringId) {
        if (Build.VERSION.SDK_INT >= 21) {
            return getResources(ctx.getApplicationContext()).getDrawable(stringId, null);
        } else {
            return getResources(ctx.getApplicationContext()).getDrawable(stringId);
        }
    }

    public static int getColor(Context ctx, int colorId) {
        return getResources(ctx.getApplicationContext()).getColor(colorId);
    }

    public static String getBuildVersion(Context ctx) {
        PackageInfo pInfo;
        try {
            pInfo = ctx.getApplicationContext().getPackageManager()
                    .getPackageInfo(ctx.getApplicationContext().getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }
}
