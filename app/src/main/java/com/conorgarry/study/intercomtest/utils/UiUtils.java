package com.conorgarry.study.intercomtest.utils;

import android.content.Context;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import com.conorgarry.study.intercomtest.App;

/**
 * Created by Conor on 13/05/2015.
 */
@SuppressWarnings("deprecation")
public class UiUtils {
    private static final String TAG = UiUtils.class.getSimpleName();

    private static int sScreenHeight = 0;
    private static int sScreenWidth = 0;
    private static int sActivityMargin = 0;

    public static float pxToDp(Context ctx, float px) {
        return px / ctx.getResources().getDisplayMetrics().density;
    }

    public static float dpToPx(Context ctx, int d) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, d,
                ctx.getResources().getDisplayMetrics());
    }

    public static float spToPx(Context ctx, int d) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, d,
                ctx.getResources().getDisplayMetrics());
    }

    public static float pxToSp(Context ctx, float px) {
        float scaledDensity = ctx.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }

    public static int getScreenWidth(Context ctx) {
        if (sScreenWidth == 0) {
            WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            sScreenWidth = Math.max(size.x, size.y);
        }

        return sScreenWidth;
    }


    public static int getScreenHeight(Context ctx) {
        if (sScreenHeight == 0) {
            WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            sScreenHeight = Math.min(size.x, size.y);
        }

        return sScreenHeight;
    }

    public static int getFullScreenMarginByPercent(Context ctx) {
        if (sActivityMargin == 0) {
            sActivityMargin = Math.max(getScreenWidth(ctx.getApplicationContext()),
                    getScreenHeight(ctx.getApplicationContext())) / 40;
        }
        return sActivityMargin;
    }



    public static void showToast(String message) {
        Toast.makeText(App.get(), message, Toast.LENGTH_SHORT).show();
    }


}
