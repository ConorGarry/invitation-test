package com.conorgarry.study.intercomtest.ui.presenters;

import com.conorgarry.study.intercomtest.models.Person;

import java.util.List;

/**
 * Created by conor on 13/11/2015.
 */
public interface IMainPresenter {
    void initGeoInvitation(String filePath);
    String getJson(String filePath);
    List<Person> parseJson(String json);
    List<Person> sortByAsc(List<Person> personList);
    double distanceFromOffice(Person person);
    List<Person> getFilteredList();
    void unSubscribe();
    double toRadians(double deg);
}
