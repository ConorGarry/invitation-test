package com.conorgarry.study.intercomtest.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.conorgarry.study.intercomtest.models.Person;
import com.conorgarry.study.intercomtest.ui.views.InviteeCell;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by conor on 14/11/2015.
 */
public class InviteAdapter extends ArrayAdapter<Person> {

    private List<Person> mPersonList;

    public InviteAdapter(WeakReference<Context> ctx, List<Person> personList) {
        super(ctx.get(), 0, personList);
        mPersonList = personList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        InviteeCell view = (InviteeCell) convertView;
        if (view == null) {
            view = new InviteeCell(parent.getContext());
        }
        view.setUserData(mPersonList.get(position).getUserId(),
                mPersonList.get(position).getDistance(), mPersonList.get(position).getName());
        // Colour every odd cell grey.
        view.setBackgroundColor(position % 2 == 0 ? Color.argb(20, 20, 20, 20) : Color.WHITE);
        return view;
    }

    @Override
    public int getCount() {
        return mPersonList.size();
    }

    @Override
    public Person getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


}
