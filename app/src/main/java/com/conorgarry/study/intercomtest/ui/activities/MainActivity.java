package com.conorgarry.study.intercomtest.ui.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.conorgarry.study.intercomtest.App;
import com.conorgarry.study.intercomtest.BuildConfig;
import com.conorgarry.study.intercomtest.ui.dialogs.DialogFactory;
import com.conorgarry.study.intercomtest.ui.views.IMainView;
import com.conorgarry.study.intercomtest.adapters.InviteAdapter;
import com.conorgarry.study.intercomtest.R;
import com.conorgarry.study.intercomtest.models.Person;
import com.conorgarry.study.intercomtest.ui.presenters.MainPresenter;
import com.conorgarry.study.intercomtest.utils.Cons;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import hugo.weaving.DebugLog;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements IMainView {

    @Bind(R.id.list_users)
    ListView listView;

    private MainPresenter mPresenter;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (BuildConfig.DEBUG) {
            Timber.d("Debug mode...");
        }

        // Init Presenter
        mPresenter = new MainPresenter(new WeakReference<>(this));

        // Init ProgressDialog Spinner
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(App.getString(getApplicationContext(),
                R.string.dialog_progress_invitations));

        // Start invitations
        mPresenter.initGeoInvitation(Cons.JSON_USERS);

        // ListView click
        listView.setOnItemClickListener((parent, view, position, id) ->
                showInviteDialog(mPresenter.getFilteredList().get(position).getName(),
                        mPresenter.getFilteredList().get(position).getDistance()));
    }


    @Override
    public void showProgress() {
        mProgressDialog.show();
    }

    @Override
    public void hideProgress() {
        mProgressDialog.dismiss();
    }

    @Override
    public void updateList(List<Person> personList) {
        listView.setAdapter(new InviteAdapter(new WeakReference<>(this), personList));
    }

    @Override
    public void showInviteDialog(String name, int distance) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        DialogFactory.DialogInvite.newInstance(name, distance).show(ft, "dialog");
    }

    @DebugLog
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Unsubscribe from Observable.
        mPresenter.unSubscribe();
    }
}
