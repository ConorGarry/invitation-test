package com.conorgarry.study.intercomtest.utils;

/**
 * Class for abstracting Constants values from other classes.
 */
public class Cons {
    public static final String JSON_USERS = "gistfile1.txt";
    public static final Double DUB_OFFICE_LAT = 53.3381985;
    public static final Double DUB_OFFICE_LON = -6.2592576;
    public static final int MAX_DISTANCE = 100;
    public static final int RADIUS_EARTH = 6371;
    public static final String REGEX_LINE_BREAK = "\\r?\\n";
}
