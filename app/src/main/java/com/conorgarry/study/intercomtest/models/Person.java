package com.conorgarry.study.intercomtest.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by conor on 13/11/2015.
 */
public class Person {
    @SerializedName("latitude")
    private String latitude;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("name")
    private String name;
    @SerializedName("longitude")
    private String longitude;

    private int distance;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return String.format("name: %s, distance: %dkm userId: %d, lat: %s, lon: %s",
                name, distance, userId, latitude, longitude);
    }
}
