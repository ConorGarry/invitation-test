package com.conorgarry.study.intercomtest;

import android.support.test.runner.AndroidJUnit4;
import android.text.format.DateUtils;
import android.view.View;

import com.conorgarry.study.intercomtest.ui.activities.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.not;

/**
 * Created by conor on 14/11/2015.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityRule<MainActivity> mActivityRule = new ActivityRule(MainActivity.class, false);

    @Before
    public void setIdlingResource() {
//        EspressoTestUtils.waitFor(DateUtils.SECOND_IN_MILLIS, true);

    }

//    @UiThreadTest
    @Test
    public void testListView() {
        mActivityRule.get(); // Launch activity
        // Allow second for listview to appear.
        // Using sleepFor which puts thread to sleep.
        // Ideally we would use waitFor which uses IdlingResources
        // but it's failing 1 in every 3/4 tests.
        EspressoTestUtils.sleepFor(DateUtils.SECOND_IN_MILLIS / 2);

        // Test 2 cells of the ListView.
        // Test dialog behaviour
        // Click first user, then OK button.
        // Click second user, then Cancel button.

        // Check if ListView is visible.
        onView(withId(R.id.list_users)).check(matches(isDisplayed()));
        // Click first item
        onData(anything()).inAdapterView(withId(R.id.list_users)).atPosition(0).perform(click());
        EspressoTestUtils.sleepFor(DateUtils.SECOND_IN_MILLIS / 2);

        // Check Dialog with OK and Cancel visible.
        onView(withText("OK")).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withText("Cancel")).inRoot(isDialog()).check(matches(isDisplayed()));

        // First entry is Ian Kehoe, 20km away.
        onView(withText("Are you sure you want to invite Ian Kehoe (10 km away)?"))
                .inRoot(isDialog()).check(matches(isDisplayed()));

        // Click Ok Button
        onView(withText("OK")).inRoot(isDialog()).perform(click());
        EspressoTestUtils.sleepFor(DateUtils.SECOND_IN_MILLIS / 2);

        // Check if dialog is gone.
        onView(withText("OK")).check(doesNotExist());

        // Check Toast popup for invitation confirmation.
        EspressoTestUtils.sleepFor(DateUtils.SECOND_IN_MILLIS / 2);
        View decorView = mActivityRule.get().getWindow().getDecorView();
        onView(withText("Ian Kehoe invited.")).inRoot(withDecorView(not(decorView)))
                .check(matches(isDisplayed()));

        // Click second item
        onData(anything()).inAdapterView(withId(R.id.list_users)).atPosition(1).perform(click());
        EspressoTestUtils.sleepFor(DateUtils.SECOND_IN_MILLIS / 2);

        // Check Dialog with OK and Cancel visible.
        onView(withText("OK")).inRoot(isDialog()).check(matches(isDisplayed()));
        onView(withText("Cancel")).inRoot(isDialog()).check(matches(isDisplayed()));

        // Second entry is Nora Dempsey, 23km away.
        onView(withText("Are you sure you want to invite Nora Dempsey (23 km away)?"))
                .inRoot(isDialog()).check(matches(isDisplayed()));

        // Click Cancel Button
        onView(withText("Cancel")).inRoot(isDialog()).perform(click());
        EspressoTestUtils.sleepFor(DateUtils.SECOND_IN_MILLIS / 2);

        // Check if dialog is gone.
        onView(withText("Cancel")).check(doesNotExist());
    }

    @After
    public void tearDown() {
        mActivityRule.get().finish();
    }


}
