package com.conorgarry.study.intercomtest;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Parcelable;
import android.support.test.InstrumentationRegistry;
import android.support.v4.util.Pair;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.ArrayList;
import java.util.List;

/**
 * A JUnit {@link Rule @Rule} which launches an activity when your test starts. Stop extending
 * gross {@code ActivityInstrumentationBarfCase2}!
 * <p/>
 * Usage:
 * <pre>{@code
 * &#064;Rule
 * public final ActivityRule<ExampleActivity> example =
 *     new ActivityRule<>(ExampleActivity.class);
 * }</pre>
 * <p/>
 * This will automatically launch the activity for each test method. The instance will also be
 * created sooner should you need to use it in a {@link Before @Before} method.
 * <p/>
 * You can also customize the way in which the activity is launched by overriding
 * {@link #getLaunchIntent(String, Class)} and customizing or replacing the {@link Intent}.
 * <pre>{@code
 * &#064;Rule
 * public final ActivityRule<ExampleActivity> example =
 *     new ActivityRule<ExampleActivity>(ExampleActivity.class) {
 *       &#064;Override
 *       protected Intent getLaunchIntent(String packageName, Class<ExampleActivity> activityClass) {
 *         Intent intent = super.getLaunchIntent(packageName, activityClass);
 *         intent.putExtra("Hello", "World!");
 *         return intent;
 *       }
 *     };
 * }</pre>
 */
public class ActivityRule<T extends Activity> implements TestRule {
    public static final String TAG = ActivityRule.class.getSimpleName();
    private final Class<T> activityClass;

    private T activity;
    private Instrumentation instrumentation;

    // My Fields
    private boolean mLaunch;
    private List<Pair<String, Object>> mIntentPairs = new ArrayList<>();

    public ActivityRule(Class<T> activityClass) {
        this.activityClass = activityClass;
        mLaunch = true;
    }

    public ActivityRule(Class<T> activityClass, boolean launch) {
        this.activityClass = activityClass;
        mLaunch = launch;
    }

    /**
     * Create the activity with an intent
     *
     * @param activityClass
     * @param intentPair
     */
    public ActivityRule(Class<T> activityClass, List<Pair<String, Object>> intentPair) {
        mIntentPairs = intentPair;
        this.activityClass = activityClass;
    }

    protected Intent getLaunchIntent(String targetPackage, Class<T> activityClass) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setClassName(targetPackage, activityClass.getName());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    /**
     * Get the running instance of the specified activity. This will launch it if it is not already
     * running.
     */
    public final T get() {
        if (mLaunch) {
            launchActivity();
        }
        return activity;
    }

    /**
     * Get the {@link Instrumentation} instance for this test.
     */
    public final Instrumentation instrumentation() {
        launchActivity();
        return instrumentation;
    }

    @Override
    public final Statement apply(final Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                launchActivity();

                base.evaluate();

                if (!activity.isFinishing()) {
                    activity.finish();
                }
                activity = null; // Eager reference kill in case someone leaked our reference.
            }
        };
    }

    public Instrumentation fetchInstrumentation() {
        Instrumentation result = instrumentation;
        return result != null ? result
                : (instrumentation = InstrumentationRegistry.getInstrumentation());
    }

    @SuppressWarnings("unchecked") // Guarded by generics at the constructor.
    private void launchActivity() {
        if (activity != null) return;

        Instrumentation instrumentation = fetchInstrumentation();

        String targetPackage = instrumentation.getTargetContext().getPackageName();
        Intent intent = getLaunchIntent(targetPackage, activityClass);

        if (mIntentPairs != null) {
            for (Pair<String, Object> p : mIntentPairs) {

                if (p.second instanceof String) {
                    intent.putExtra(p.first, (String) p.second);
                } else if (p.second instanceof Parcelable) {
                    intent.putExtra(p.first, (Parcelable) p.second);
                } else if (p.second instanceof Integer) {
                    intent.putExtra(p.first, (int) p.second);
                } else if (p.second instanceof Boolean) {
                    intent.putExtra(p.first, (boolean) p.second);
                } else if (p.second instanceof Long) {
                    intent.putExtra(p.first, (long) p.second);
                } else if (p.second instanceof Double) {
                    intent.putExtra(p.first, (double) p.second);
                } else if (p.second instanceof Short) {
                    intent.putExtra(p.first, (short) p.second);
                } else if (p.second instanceof Float) {
                    intent.putExtra(p.first, (float) p.second);
                } else if (p.second instanceof Integer[]) {
                    intent.putExtra(p.first, (int[]) p.second);
                } else if (p.second instanceof String[]) {
                    intent.putExtra(p.first, (String[]) p.second);
                }

            }
        }

        activity = (T) instrumentation.startActivitySync(intent);
        instrumentation.waitForIdleSync();
    }
}