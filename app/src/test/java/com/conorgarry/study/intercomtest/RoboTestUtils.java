package com.conorgarry.study.intercomtest;

import org.robolectric.Robolectric;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by conor on 14/11/2015.
 */
public class RoboTestUtils {
    public static void robolectricSleep(long sleepTime) {
        Robolectric.flushBackgroundThreadScheduler();
        Robolectric.getBackgroundThreadScheduler().idleConstantly(true);

        // A latch used to lock UI Thread.
        final CountDownLatch lock = new CountDownLatch(1);

        try
        {
            lock.await(sleepTime, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e)
        {
            lock.notifyAll();
        }

        // Flush all UI tasks out of queue and force them to execute.
        Robolectric.flushForegroundThreadScheduler();
        Robolectric.getForegroundThreadScheduler().idleConstantly(true);
    }
}
