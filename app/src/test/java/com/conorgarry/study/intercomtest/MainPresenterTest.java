package com.conorgarry.study.intercomtest;

import android.test.UiThreadTest;
import android.text.TextUtils;
import android.text.format.DateUtils;

import com.conorgarry.study.intercomtest.models.Person;
import com.conorgarry.study.intercomtest.ui.presenters.IMainPresenter;
import com.conorgarry.study.intercomtest.ui.presenters.MainPresenter;
import com.conorgarry.study.intercomtest.ui.views.IMainView;
import com.conorgarry.study.intercomtest.utils.Cons;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertTrue;


/**
 * Created by conor on 14/11/2015.
 * Testing the logic of the MainPresenter.
 * Created a file "mock_gistfile1.txt" with 7 names randomly selected from original list,
 * this way we can check the the integrity of the presenter methods knowing the results.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MainPresenterTest {

    public static final String MOCK_JSON_USERS = "mock_gistfile1.txt";

    @Mock
    IMainView mainView;
    IMainPresenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        presenter = new MainPresenter(new WeakReference<>(mainView));
    }

    @Test
    public void loadTestDataFromResources() {
        // Check file exists in assets
        boolean fileExists;
        try {
            fileExists = Arrays.asList(RuntimeEnvironment.application.getAssets().list("")).contains(MOCK_JSON_USERS);
        } catch (IOException e) {
            fileExists = false;
            e.printStackTrace();
        }
        assertTrue("File should exist in asset folder", fileExists);

        // Check json string is not null.
        assertNotNull("Result should not be null from getJson", presenter.getJson(MOCK_JSON_USERS));

        // Check file is valid json
        assertTrue("File should have valid JSON format",
                TestUtils.isJSONValid(presenter.getJson(MOCK_JSON_USERS)));
    }

    @Test
    public void testParse() {
        List<Person> personList = presenter.parseJson(presenter.getJson(MOCK_JSON_USERS));

        // Check Person objects are being created and added to List
        assertNotNull("List not created from parseJson", personList);
        assertTrue("List has not been populated with users", personList.size() > 0);

        // Null checks
        for (int i = 0; i < personList.size(); i++) {
            assertNotNull(String.format("Name is null index: %d", i), personList.get(i).getName());
            assertNotNull(String.format("Id is null index: %d", i), personList.get(i).getUserId());
            assertNotNull(String.format("Latitude is null index: %d", i), personList.get(i).getLatitude());
            assertNotNull(String.format("Longitude is null index: %d", i), personList.get(i).getLongitude());
        }

        // Check Person items are valid withing the List, we'll check first and last entries.
        Person pFirst = personList.get(0); // Jack Enright, lat:52.3191841, lon:-8.5072391, id:3
        assertTrue("Name should equal 'Jack Enright", TextUtils.equals(pFirst.getName(), "Jack Enright"));
        assertTrue("Latitude should equal '52.3191841", TextUtils.equals(pFirst.getLatitude(), "52.3191841"));
        assertTrue("Longitude should equal '-8.5072391", TextUtils.equals(pFirst.getLongitude(), "-8.5072391"));
        assertEquals("id should equal 3", 3, pFirst.getUserId());

        Person pLast = personList.get(personList.size() - 1); // David Behan, lat:52.833502, lon:-8.522366, id:25
        assertTrue("Name should equal 'David Behan'", TextUtils.equals(pLast.getName(), "David Behan"));
        assertTrue("Latitude should equal '52.833502'", TextUtils.equals(pLast.getLatitude(), "52.833502"));
        assertTrue("Longitude should equal '-8.522366'", TextUtils.equals(pLast.getLongitude(), "-8.522366"));
        assertEquals("id should equal 25", 25, pLast.getUserId());
    }

    // There are 7 users in the mocked list.
    @Test
    public void testListNumber() {
        assertEquals("Result should equal 7", 7, presenter.parseJson(presenter.getJson(MOCK_JSON_USERS)).size());
    }

    // Test overall stream of data.
    // We're not using the mocked list,
    @Test
    @UiThreadTest
    public void testFullInviteMethod() {
        presenter.initGeoInvitation(Cons.JSON_USERS);

        // Hold off for 1 second, allow rx stream to complete,
        // then get < 100km filtered List from presenter.
        RoboTestUtils.robolectricSleep(DateUtils.SECOND_IN_MILLIS);
        List<Person> filteredList = presenter.getFilteredList();

        // Check not null and not empty
        assertTrue("Filtered list should not be null or empty",
                filteredList != null && !filteredList.isEmpty());

        // We know the result should be 12
        assertEquals("Filtered list should equal 12", 12, filteredList.size());

        // We'll check first and last entries.
        assertTrue("First filtered user should be Ian Kehoe",
                TextUtils.equals(filteredList.get(0).getName(), "Ian Kehoe"));
        assertTrue("Last filtered user should be Lisa Ahearn",
                TextUtils.equals(filteredList.get(filteredList.size() - 1).getName(), "Lisa Ahearn"));
    }

    @Test
    public void testPresenter() {
        assertTrue("Presenter should not be null", presenter != null);
    }

    @Test
    public void testConversion() {
        assertEquals("180 degrees should equal Pi radians", Math.PI, presenter.toRadians(180));
    }
}
